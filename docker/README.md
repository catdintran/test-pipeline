# Docker files for UIP

## Dockerfile

The UIP's Dockerfile pulls our python 3.8 base image from the AWS helm-production account's ECR and builds the image using the build.sh script in the repo's root directory.

To manually build the image, from the repo's root directory run `docker build -f docker/Dockerfile . ` .

## Docker Compose

For local development, the docker-compose file spins up all current microservices and dependencies. This compose file aims to mimic our current UIP prod architecture.

To inject the necessary UIP's environment variables into the docker images, all services import a `.env` file. This repo contains an example of this file called `.env.example`.

First prepare local configurations, perform the following from the repo's root directory:
- Use the example .env file for all the local services: `cp .env.example .env`
- If you have a config.yml file, back it up first: `cp config.yml config.yml.backup`
- Use the example config.yml file so the preprocessor test command can locate the local services: `cp config.yml.example config.yml`

Then to start the images: `docker-compose -f docker/docker-compose.yml up`. (don't forget you need the docker app running!)
You'll see a bunch of kafka & mongo initialization logs, that's expected. 

Finally, send data to it using the preprocessor: `'python3 -m commands.preprocessor --data_file=./tests/assets/test_files/test_input.csv --config_file=./tests/assets/test_files/test_config.json'`.

Troubleshooting:
- If you get an error regarding `"No such file or directory: './core-data-model/files/fields.json'"` you may first need to update the repo's submodules: `git submodule update --init --recursive`

### Services

Our docker-compose starts 6 services: the 3 UIP's microservices (mapper, normalizer, persister) and 3 of their necessary dependency services (zookeeper, kafka and mongodb).

The 3 UIP microservices use the standard UIP base image from ECR, but they mount the current uip/ directory into the container. Therefore any local changes to the codebase will be applied to the images. 

The mongodb service use the standard mongo image from docker hub. The zookeeper and kafka images are pulled from [Confluent](https://www.confluent.io/). The extra variables we inject into these images are logging-related, so that we only see warning & error logs.
